import firebase from "firebase/app";
import "firebase/auth";
import "firebase/analytics";
import { firebaseConfig } from "./config";

firebase.initializeApp(firebaseConfig);

const verifyPassword = () => {
    let passwordElement = document.getElementById("pass-input");
    let verifiedPasswordElement = document.getElementById("verify-pass-input");

    if (!passwordElement.value || !verifiedPasswordElement.value) return false;

    return passwordElement.value === verifiedPasswordElement.value; // seriously the best idea I could think of ¯\_(ツ)_/¯
};

const handleSignUpError = (error) => {
    // TODO: Extend error handling
    if (error.code === "auth/email-already-in-use")
        return console.log("email already in use");

    if (error.code === "auth/weak-password")
        return console.log(
            "boi your password is weak af, it has to be at least 6 characters smhhhhhh"
        );

    console.error(error);
};

const signUp = (email, password) => {
    firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(() => console.log("signed up"))
        .catch(handleSignUpError);
};

const handleSignUpFromKeyPress = (e) => {
    if (e.key !== "Enter") return;
    let emailElement = document.getElementById("email-input");
    let passwordElement = document.getElementById("pass-input");

    if (!verifyPassword()) return console.log("Passwords do not match");

    signUp(emailElement.value, passwordElement.value);
};

const handleSignUpFromButtonPress = () => {
    let emailElement = document.getElementById("email-input");
    let passwordElement = document.getElementById("pass-input");

    if (!verifyPassword()) return console.log("Passwords do not match");

    signUp(emailElement.value, passwordElement.value);
};

const main = () => {
    let passInput = document.getElementById("pass-input");
    let verifyPassInput = document.getElementById("verify-pass-input");

    passInput.onkeypress = handleSignUpFromKeyPress;
    verifyPassInput.onkeypress = handleSignUpFromKeyPress;

    let submitButton = document.getElementById("pass-submit-button");
    submitButton.onclick = handleSignUpFromButtonPress;

    firebase.analytics().logEvent("opened_page");
};

window.onload = main;