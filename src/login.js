import firebase from "firebase/app";
import "firebase/auth";
import { firebaseConfig, MAIN_PAGE_URL } from "./config";
import redirectTo from "./utils/redirect";

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();

const handleSuccessfulSignUp = (data) => {
  window.sessionStorage.setItem("credentials", JSON.stringify(data))
  console.log("signed up using facebook");
  redirectTo(MAIN_PAGE_URL);
}

const signUpWithFacebook = () => {
  const provider = new firebase.auth.FacebookAuthProvider();
  auth.signInWithPopup(provider).then(handleSuccessfulSignUp).catch(console.error);
};

const hookListeners = () => {
  document.getElementById("fb").onclick = signUpWithFacebook;
};

window.onload = hookListeners;
