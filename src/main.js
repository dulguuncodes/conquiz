import firebase from "firebase/app";
import "firebase/firestore";
import { firebaseConfig } from "./config";
import { getDateTiming } from "./utils/date";

firebase.initializeApp(firebaseConfig);

const handleNewGameData = (snapshot) => {
  const container = document.getElementById("bodie");
  if (!container) return; // shouldn't happen unless some kid manipulates the DOM

  container.innerHTML = ""; // clears container, removing old elements

  snapshot.forEach((res) => {
    let data = res.data();
    let game = document.createElement("div");
    game.id = "game";

    if (data.startingAt.toDate() < new Date())
      game.style.backgroundColor = "green";

    const header = Object.assign(document.createElement("h1"), {
      innerHTML: `Grade ${data.grade}`,
      className: "grade",
    });

    const startingAt = Object.assign(document.createElement("h1"), {
      innerHTML: `${getDateTiming(data.startingAt.toDate())}`,
      className: "starting-at",
    });

    game.appendChild(header);
    game.appendChild(startingAt);
    container.appendChild(game);
  });
};

window.onload = () => {
  const database = firebase.firestore();
  database
    .collection("games")
    .onSnapshot((snapshot) => handleNewGameData(snapshot));
};
