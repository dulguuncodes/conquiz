import firebase from "firebase/app";
import "firebase/firestore";
import { firebaseConfig } from "./config";

firebase.initializeApp(firebaseConfig);

const database = firebase.firestore();

const quizID = new URLSearchParams(window.location.search).get("quiz");
const questionID = new URLSearchParams(window.location.search).get("question");

const cacheQuestions = (data, questionID) => {
  const documents = data.docs;
  const questions = documents.map((raw) => raw.data());

  if (window.localStorage.getItem("questions")) return;

  window.localStorage.setItem("questions", JSON.stringify(questions));
};

const loadQuestionCount = (snap) => {
  const data = snap.data();
  document.getElementById("num").innerHTML = `1/${data.length}`;
};

const cacheMetadata = (snap) => {
  const data = snap.data();

  if (window.localStorage.getItem(`${questionID}-metadata`)) return;

  window.localStorage.setItem(`${questionID}-metadata`, JSON.stringify(data));
};

const handleCorrectAnswer = () => {
  alert("You got it right!");
};

const handleWrongAnswer = () => {
  alert("You got it wrong :(( smhhh");
};

const handleAnswerPress = (e) => {
  const answerID = e.target.id;
  database
    .collection("quizzes")
    .doc(quizID)
    .collection("questions")
    .doc(questionID)
    .get()
    .then((snap) => {
      const data = snap.data();
      const answer = data.answers.filter((answer) => answer.id === answerID);
      if (!answer) return;
      if (answer[0].correct) return handleCorrectAnswer();
      handleWrongAnswer();
    });
};

const loadQuestion = (snap, questionID) => {
  const cache = window.localStorage.getItem(questionID);
  const question = cache
    ? JSON.parse(cache).filter((val) => val.id === questionID)
    : snap.data();

  console.log(question);

  document.getElementById("question").innerHTML = question.title;
};

const loadAnswers = (snap) => {
  const answers = snap.data().answers;
  const half = Math.ceil(answers.length / 2);

  const firstHalf = answers.slice(0, half);
  const secondHalf = answers.slice(-half);

  const firstHalfElement = Object.assign(document.createElement("div"), {
    className: "ans-half",
  });

  const secondHalfElement = Object.assign(document.createElement("div"), {
    className: "ans-half",
  });

  firstHalf.forEach((data, index) => {
    const answer = Object.assign(document.createElement("button"), {
      innerHTML: data.value,
      className: `answers${index + 3}`,
      id: data.id,
    });

    answer.onclick = handleAnswerPress;

    firstHalfElement.appendChild(answer);
  });

  secondHalf.forEach((data, index) => {
    const answer = Object.assign(document.createElement("button"), {
      innerHTML: data.value,
      className: `answers${index + 1}`,
      id: data.id,
    });

    answer.onclick = handleAnswerPress;

    secondHalfElement.appendChild(answer);
  });

  document.getElementById("answers-box").appendChild(firstHalfElement);
  document.getElementById("answers-box").appendChild(secondHalfElement);
};

database
  .collection("quizzes")
  .doc(quizID)
  .get()
  .then((snap) => {
    loadQuestionCount(snap);
    cacheMetadata(snap);
  });

database
  .collection("quizzes")
  .doc(quizID)
  .collection("questions")
  .get()
  .then((snap) => {
    cacheQuestions(snap, questionID);
  });

database
  .collection("quizzes")
  .doc(quizID)
  .collection("questions")
  .doc(questionID)
  .get()
  .then((snap) => {
    loadQuestion(snap, questionID);
    loadAnswers(snap);
  });
