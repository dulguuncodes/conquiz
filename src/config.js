const firebaseConfig = {
  apiKey: "AIzaSyAL4fobbPo5u6PJ1wz2KLcVoT1unxG88Uk",
  authDomain: "quiztest-1c261.firebaseapp.com",
  projectId: "quiztest-1c261",
  storageBucket: "quiztest-1c261.appspot.com",
  messagingSenderId: "354567896611",
  appId: "1:354567896611:web:3f936ceaee4c4437e03b96",
  measurementId: "G-V6PGYEVX3J",
};

const BASE_URL = process.env.BASE_URL ?? "http://localhost:5000";

const MAIN_PAGE = process.env.MAIN_PAGE ?? "./main.html";
const MAIN_PAGE_URL = new URL(MAIN_PAGE, BASE_URL).href;

export { firebaseConfig, MAIN_PAGE, MAIN_PAGE_URL };
