const fetchTomorrowsDate = (date) => {
  const newDate = new Date(date);
  newDate.setDate(newDate.getDate() + 1);
  return newDate;
};

const getDateTiming = (date) => {
  const current = new Date();
  if (date <= current) return "Now";
  if (date <= fetchTomorrowsDate(current)) return "Tomorrow";
  return "unknown";
};

module.exports = {
  getDateTiming,
  fetchTomorrowsDate,
};
