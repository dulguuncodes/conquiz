# qtest

qtest is one of the world's leadest pioneers in quizzes. we are here to make history with one of the worst mobile apps :^)

## Requirements

1. You **must** have yarn installed, if you unsure run `yarn --version`
   If you do not have yarn, install it using `sudo npm i -g yarn` or `npm i -g yarn`

2. You must have Node.js installed, install it [here](https://nodejs.org/en/download/) or if you use Linux
   install it via your package manager

3. Firebase CLI Tools. We require this so we can serve the website locally
   You can install it using `npm i -g firebase-cli` (note that it requires node version >= 15)

## Running locally

1. Install Dependencies using `yarn`

2. Build Javascript files - `yarn build`

3. Serve the files and start a local server with `yarn serve`
   The default port should be **localhost:5000**

## Contribution

When commiting changes, make sure you run tests before you push with `yarn test`,
we also have commitizen configured, so we encourage using it to commit changes, you
can do so with `yarn commit`

Read [CONTRIBUTION.md](./CONTRIBUTION.md) for more details on how to contribute to the project

