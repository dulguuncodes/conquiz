const path = require("path");
const webpack = require("webpack");
const TerserPlugin = require("terser-webpack-plugin");
const PurgeCSSPlugin = require("purgecss-webpack-plugin");
const glob = require("glob");

const PATHS = {
  public: path.join(__dirname, "public"),
  src: path.join(__dirname, "src"),
};

module.exports = {
  mode: "development",
  entry: {
    acc_create: "./src/acc-create.js",
    main: "./src/main.js",
    quiz_page: "./src/quiz-page.js",
    login: "./src/login.js",
  },
  output: {
    path: path.resolve(__dirname, "public"),
    filename: "[name].bundle.js",
  },
  resolve: {
    extensions: [".js"],
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin(),
      new PurgeCSSPlugin({
        paths: glob.sync(`${PATHS.src}/**/*`, { nodir: true }),
      }),
    ],
  },
  plugins: [
    new webpack.EnvironmentPlugin({
      MAIN_PAGE: "./main.html",
      BASE_URL: "http://localhost:5000",
    }),
  ],
};
