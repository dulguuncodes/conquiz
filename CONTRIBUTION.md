# Contributing to qtest

## Commiting Guidelines

Make sure all commits are properly formatted, if you are ensure run `yarn commit:lint` to
see any mistakes. If there is an error with your previous commit, you can amend it with
`yarn commit --amend`

Changes made to Javascript and HTML files will be reviewed by @dulguuncodes, while the
CSS will be reviewed by @TsogtOnCrack

## Making a PR

While making a Pull Request, please keep in mind that we may not see your PR in time as
we may have a time zone difference. Please be patient as we both clone your changes as well
as test them.

